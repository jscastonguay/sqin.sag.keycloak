### Keycloak configuration file

### Variables
user='admin'
pwd='admin'
REALM_SAG='sag'
$KEYCLOAK_HOME = "/opt/jboss/keycloak"

### Init
export PATH=$PATH:$KEYCLOAK_HOME/bin

### Authentication
echo "Authentication"
kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user $user --password $pwd

### Realm creation
echo "creating realm $REALM_SAG
kcadm.sh create realms -s realm=$REALM_SAG -s enabled=true

### Themes ###
# echo Thmes
# kcadm.sh update realms/$REALM_SAG -s accountTheme=
# kcadm.sh update realms/$REALM_SAG -s adminTheme=
k cadm.sh update realms/$REALM_SAG -s enab$realmSAG