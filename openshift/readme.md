# Configuration de keycloak sous Openshift

Référence : https://github.com/keycloak/keycloak-containers/blob/master/server/README.md

## Prérequis

* Créer un personnal acces token dans GitLab ayant les droits de lecture sur le 
repo qui contient le fichier docker qui crée l'image Keycloak personalisée pour 
le SAG (idéalement utiliser un compte de service).
* Créer un Source Secret dans Openshift avec le personnel acces token de gitlab. 
Laisser d'utilisateur vide.
* (ex : forge-gitlab-phil-pull-secret).
* Un certificat SSL (pour un environnement d'essai vous pouvez avoir un certificat de ce site https://www.sslforfree.com/)

## Installation

En tant qu'administrateur, créer un nouveau projet Openshift.

```
oc login --token=<TOKEN_ADMIN> --server=<ADRESSE_SERVEUR_OPENSHIFT>

oc new-project <NOM_PROJET>
```
Exemple:
```
oc new-project accesuniqc-sag
```

## Importation du gabarit Openshift pour Keycloak

Exécuter la commande d'importation en utilisant le fichier ["keycloak-https"](/openshift/keycloak-https.json) qui
contient le gabarit openshift pour keycloak : 

```
oc new-app -p NAMESPACE="NOM_PROJET" -f keycloak-https.json

```

Exemple : 
```
oc new-app -p NAMESPACE="accesuniqc-sag" -f keycloak-https.json
```

## Délégation des droits d'administrateurs pour le projet
Donner les droits aux administrateurs du projet aux membres de l'équipe de 
développement 

Commande : 
```
oc adm policy add-role-to-user admin <NOM_UTILISATEUR> -n accesuniqc-sag
```

Exemple : 
```
oc adm policy add-role-to-user admin rjop005 -n accesuniqc-sag
```

## Exportation du template openshift du SAG


> Pour exporter le projet et toutes ses dépendance, utilisé le script "export.sh" |


Sélectionner le projet accesuniqc-sag-keycloak.
```
$ oc project accesuniqc-sag
```

Exportation du projet accesuniqc-sag-keycloak.
```
$ oc get -o yaml --export all > accesuniqc-sag-keycloak.yaml
```

Exportation des secrets.
```
oc get -o yaml secrets keycloak-admin-secrets > keycloak-admin-secrets.yaml
oc get -o yaml secrets gitlab-forge-qc-ca-rjop005-token > gitlab-forge-qc-ca-rjop005-token.yaml
```

Exportation des config maps.

```
 oc get -o yaml configmaps keycloak-db-vendors > keycloak-db-vendors.yaml
 oc get -o yaml configmaps keycloak-loglevels > keycloak-loglevels.yaml
```
