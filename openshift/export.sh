echo Exportation du projet accesuniqc-sag 

oc project accesuniqc-sag

oc get -o yaml --export all > project.yaml

for object in rolebindings serviceaccounts secrets imagestreamtags cm egressnetworkpolicies rolebindingrestrictions limitranges resourcequotas pvc templates cronjobs statefulsets hpa deployments replicasets poddisruptionbudget endpoints
do
  oc get -o yaml --export $object > $object.yaml
done
