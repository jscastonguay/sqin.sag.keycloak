echo "Importation du projet accesuniqc-sag"
oc new-project accesuniqc-sag
oc create -f project.yaml
oc create -f secrets.yaml
oc create -f serviceaccounts.yaml
oc create -f pvc.yaml
oc create -f rolebindings.yaml