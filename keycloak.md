# Configure une instance de test de Keycloak

## Information de l'instance keycloak

[URL administration](https://127.0.0.1/auth/)
[URL login realm AccesUniQC](https://127.0.0.1/auth/realms/AccesUniQC/account)


## Commandes d'administration du client Keycloak

[Documentation officielle](https://github.com/keycloak/keycloak-documentation/blob/master/server_admin/topics/export-import.adoc)

### Configuration initiale

Une authentification initiale est requise avant de pouvoir exécuter des commandes

#### Commande :
`
kcadm.bat config credentials --server <URL du serveur> --realm master --user admin --password admin
`

#### Exemples :
`
$ kcadm.sh config credentials --server https://127.0.0.1/auth/ --realm master --user admin --password admin
`
`
$ kcadm.bat config credentials --server http://127.0.0.1:8080/auth --realm master --user admin --password admin
`

### Exportation d'un Realm

#### Commande :
`
$ kcadm.sh get realms/<Nom du Realm> > <Path>/<NomFichier>.json
`
#### Exemples :
`
$ kcadm.sh get realms/AccesUniQC > ../Export/AccesUniQCExport.json
`

###Importation d'un Realm

#### Commande :
`
$ kcadm.sh update realms/<Nom du Realm> -f <Path>/<NomFichier>.json
`
#### Exemples :
`
$ kcadm.sh update realms/AccesUniQC -f ../Export/AccesUniQCExport.json
`


# Templating
[Freemaker](https://freemarker.apache.org/)

# Exemples
[raincatcher template](https://github.com/austincunningham/raincatcher-keycloak-theme)