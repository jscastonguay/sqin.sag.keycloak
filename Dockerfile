FROM jboss/keycloak AS base
MAINTAINER Philippe Jobin

#ENV KEYCLOAK_IMPORT=$KEYCLOAK_IMPORT="/importdata/accesuniqc/AccesUniQC-realm.json,/importdata/accesuniqc/AccesUniQC-users-0.json"
ENV KEYCLOAK_USER=admin
ENV KEYCLOAK_PASSWORD=admin

WORKDIR /opt/jboss/keycloak
COPY ["/themes", "./themes"]
RUN mkdir ./importdata
COPY ["/realms/", "./importdata"]