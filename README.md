# Installation Keycloak

Ce projet permet de configurer une instance Keycloak de test dans openshift  

# URL 

## localhost
Realm Keycloak :
* [Home](http://localhost:8080/auth/)
* [Console d'administration](http://localhost:8080/auth/admin/)

Realm accesuniqc
* [Compte](http://localhost:8080/auth/realms/accesuniqc/account)


## Openshift

Realm Keycloak :
* [Home](https://keycloak-accesuniqc-sag.apps.cqendev.pocquebec.org/auth/)
* [Console d'administration](https://keycloak-accesuniqc-sag.apps.cqendev.pocquebec.org/auth/admin/)

Realm accesuniqc
* [Compte](https://keycloak-accesuniqc-sag.apps.cqendev.pocquebec.org/auth/realms/accesuniqc/account)

# Configure une instance de test de Keycloak

## Information de l'instance keycloak

* [URL administration](https://127.0.0.1/auth/)
* [URL login realm AccesUniQC](https://127.0.0.1/auth/realms/AccesUniQC/account)


## Import / Export

[Documentation officielle](https://github.com/keycloak/keycloak-documentation/blob/master/server_admin/topics/export-import.adoc)


# Templating
Keycloak permet de personnaliser ses pages à l'aide de l'outil Freemaker.

## Documentation
* [Documentation officielle](https://www.keycloak.org/docs/latest/server_development/#_themes)
* [Freemaker](https://freemarker.apache.org/)

## Exemples
* [raincatcher template](https://github.com/austincunningham/raincatcher-keycloak-theme)